create table mango_dev.sys_account
(
    id        bigint auto_increment comment '主键'
        primary key,
    username  varchar(64)                             null comment '账号',
    password  varchar(64)                             not null comment '密码',
    tel       varchar(16)                  default '' null comment '手机号',
    user_type tinyint                                 null comment '用户类型',
    nick_name varchar(64)                  default '' null comment '昵称',
    enable    tinyint(4) unsigned zerofill default 0  null comment '可用状态',
    del       tinyint(4) unsigned zerofill default 0  null comment '删除标记',
    remark    varchar(256)                            null comment '备注'
)
    comment '账户表';

