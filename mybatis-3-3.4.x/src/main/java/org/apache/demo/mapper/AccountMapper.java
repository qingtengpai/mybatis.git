package org.apache.demo.mapper;

import org.apache.demo.Account;

public interface AccountMapper {

    Account selectById(Long id);
}
